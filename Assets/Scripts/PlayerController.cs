﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float speed = 2f;
	public float jumpForce = 10f;
	public float gravityScale = 1f;

	public RobbedInterface currentRobbingEntity =  null;

	CharacterController characterController;
	Animator animator;

	private Vector3 moveDirection;
	// Use this for initialization
	void Start () {
		characterController = GetComponent<CharacterController>();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		Move();
	}

	void Move() {
		moveDirection = new Vector3(Input.GetAxis("Horizontal") * speed, moveDirection.y, 0);

		if (characterController.isGrounded) {
			if (Input.GetButtonDown("Jump")) {
				moveDirection.y = jumpForce;
			}
		}

		moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);

		characterController.Move(moveDirection * Time.deltaTime);
		SetAnimation(moveDirection);
	}

	void SetAnimation(Vector3 moveDirection) {
		if (Input.GetAxis("Horizontal") == 0) {
			animator.SetBool("Walk", false);
		} else {
			animator.SetBool("Walk", true);
		}
	}
	

	void OnTriggerStay(Collider other) {
		Debug.Log("FOUND");
		if (other.CompareTag("RobEntity")) {
			
			currentRobbingEntity = other.GetComponent<RobbedInterface>();
			if (currentRobbingEntity != null) {
				currentRobbingEntity.HighlightMaterial();
			}
		}
	}

	/// <summary>
	/// OnTriggerExit is called when the Collider other has stopped touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("RobEntity")) {
			RobbedInterface exitedRobbedInterface = other.GetComponent<RobbedInterface>();
			if (exitedRobbedInterface.Equals(currentRobbingEntity)) {	
				currentRobbingEntity.UnHighlightMaterial();
				currentRobbingEntity = null;
			}
		}
	}

}
