﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface RobbedInterface {

	void Steal(float amount);
	float CheckAmount();

	void HighlightMaterial();
	void UnHighlightMaterial();

}
