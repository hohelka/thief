﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LadyInterface : MonoBehaviour, RobbedInterface {


	public float amount = 1000f;
	private Text amountText;

    // Use this for initialization
    void Start () {
		amountText = transform.Find("StealMoney").Find("Text").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void RefreshText() {
		amountText.text = amount + "$";
	}

	public float CheckAmount()
    {
        return amount;
    }

    public void Steal(float amount)
    {
        this.amount -= amount;
		RefreshText();
    }

    public void HighlightMaterial()
    {
        GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
    }

    public void UnHighlightMaterial()
    {
        GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
    }
}
